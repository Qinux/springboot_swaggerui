package com.shelton.swaggerui.controller;

import com.shelton.swaggerui.entity.User;
import io.swagger.annotations.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@Api(value = "用户接口", tags={"用户操作接口"})
public class IndexController {

    @ApiOperation(value = "用户登录",notes = "登录",httpMethod = "POST")
    @PostMapping("/login")
    public Object login(@RequestParam @ApiParam(name="username",value="登录账号",required=true) String username, @RequestParam @ApiParam(name="password",value="密码",required=true) String password){

        Map map = new HashMap();
        map.put("msg","success");
        map.put("code","1");
        return map;
    }

    @RequestMapping(value = "/addUser",method = RequestMethod.POST)
    @ApiModelProperty(value="user",notes = "用户信息的json串")
    @ApiOperation(value = "新增用户", notes="返回新增的用户信息")
    public Object adduser(@RequestBody User user){

        Map map = new HashMap();
        map.put("msg","success");
        map.put("code","1");
        return map;
    }



    @RequestMapping(value = "/byname", method = RequestMethod.GET)
    @ResponseBody
    @ApiImplicitParam(paramType = "query",name= "username" ,value = "用户名",dataType = "string")
    @ApiOperation(value = "通过用户名获取用户信息", notes="返回用户信息")
    public Object byname(@RequestParam @ApiParam(name="username",value="名字",required = true)String username) {
        Map map = new HashMap();
        map.put("msg","success");
        map.put("code","1");
        return map;

    }


    @RequestMapping(value = "/deleteUser", method = RequestMethod.DELETE)
    @ResponseBody
    @ApiOperation(value = "通过id删除用户信息", notes="返回删除状态1 成功 0 失败")
    public Map<String,Object> deleteUser(@RequestParam @ApiParam(name="id",value="id",required = true) Integer id) {
        Map map = new HashMap();
        map.put("msg","success");
        map.put("code","1");
        return map;
    }


    @RequestMapping(value = "/updateUser",method = RequestMethod.PUT)
    @ResponseBody
    @ApiModelProperty(value="user",notes = "修改后用户信息的json串")
    @ApiOperation(value = "更新用户信息", notes="返回更新的用户信息")
    public Map<String,Object> updateUser(@RequestBody User user) {

        Map map = new HashMap();
        map.put("msg","success");
        map.put("code","1");
        return map;
    }


}
